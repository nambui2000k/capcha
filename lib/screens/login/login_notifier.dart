

import 'dart:convert';

import 'package:capcha/core/public/global.dart';
import 'package:capcha/core/widgets/dialog/dialog_alert.dart';
import 'package:capcha/models/captcha_model.dart';
import 'package:capcha/screens/application/application_service.dart';
import 'package:capcha/screens/home/home_screen.dart';
import 'package:capcha/screens/login/login_service.dart';
import 'package:crypto/crypto.dart';
import 'package:flutter/material.dart';

class LoginNotifier extends ChangeNotifier {
  TextEditingController usernameCtrl = TextEditingController();
  TextEditingController passwordCtrl = TextEditingController();
  TextEditingController captchaCtrl = TextEditingController();


  CaptchaModel? captchaModel;
  LoginNotifier(BuildContext context){
    Future.delayed(Duration.zero,() async {
      getCaptCha(context);
    });
  }
  Future<void> getCaptCha(BuildContext context) async {
    captchaModel = await applicationService.getCaptcha(context);
    notifyListeners();
  }

  Future<void> onLogin(BuildContext context) async {
    FocusScope.of(context).unfocus();
    usernameCtrl.text = usernameCtrl.text.trim();
    passwordCtrl.text = passwordCtrl.text.trim();
    captchaCtrl.text = captchaCtrl.text.trim();
    if(usernameCtrl.text.isEmpty){
      DialogAlert.showNotification(context, "Thông báo", "Vui lòng nhập tên đăng nhập");
      return;
    }
    if(passwordCtrl.text.isEmpty){
      DialogAlert.showNotification(context, "Thông báo", "Vui lòng nhập mật khẩu");
      return;
    }
    if(captchaCtrl.text.isEmpty){
      DialogAlert.showNotification(context, "Thông báo", "Vui lòng nhập mã captcha");
      return;
    }

    Map<String,dynamic> body = {
      "loginname": usernameCtrl.text,
      "loginpassword": Hmac(sha1, utf8.encode(usernameCtrl.text)).convert(utf8.encode(passwordCtrl.text)).toString(),
      "captcha": captchaCtrl.text,
      "captchauuid": captchaModel!.uuid,
      "fingerprint": "",
      "portalid": ""
    };


    dynamic result = await loginService.login(context, body);
    if(result!=null){
      Global.token = result['token'];
      Navigator.pushNamedAndRemoveUntil(context, HomeScreen.routeName, (route) => false);
    }else{
      getCaptCha(context);
    }

  }
}

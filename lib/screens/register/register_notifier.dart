

import 'dart:convert';

import 'package:capcha/core/public/global.dart';
import 'package:capcha/core/public/global.dart';
import 'package:capcha/core/ulitis/utils.dart';
import 'package:capcha/core/widgets/dialog/dialog_alert.dart';
import 'package:capcha/models/captcha_model.dart';
import 'package:capcha/screens/application/application_service.dart';
import 'package:capcha/screens/register/register_service.dart';
import 'package:crypto/crypto.dart';
import 'package:flutter/material.dart';

class RegisterNotifier extends ChangeNotifier {
  TextEditingController usernameCtrl = TextEditingController();
  TextEditingController phoneNumberCtrl = TextEditingController();
  TextEditingController passwordCtrl = TextEditingController();
  TextEditingController captchaCtrl = TextEditingController();

  CaptchaModel? captchaModel;
  RegisterNotifier(BuildContext context){
    Future.delayed(Duration.zero,() async {
      getCaptCha(context);
    });
  }
  Future<void> getCaptCha(BuildContext context) async {
    captchaModel = await applicationService.getCaptcha(context);
    notifyListeners();
  }

  Future<void> onRegister(BuildContext context) async {
    FocusScope.of(context).unfocus();
    usernameCtrl.text = usernameCtrl.text.trim();
    phoneNumberCtrl.text = phoneNumberCtrl.text.trim();
    passwordCtrl.text = passwordCtrl.text.trim();
    captchaCtrl.text = captchaCtrl.text.trim();
    if(usernameCtrl.text.isEmpty){
      DialogAlert.showNotification(context, "Thông báo", "Vui lòng nhập tên đăng nhập");
      return;
    }
    if(phoneNumberCtrl.text.isEmpty){
      DialogAlert.showNotification(context, "Thông báo", "Vui lòng nhập số điện thoại");
      return;
    }
    if(passwordCtrl.text.isEmpty){
      DialogAlert.showNotification(context, "Thông báo", "Vui lòng nhập mật khẩu");
      return;
    }
    if(captchaCtrl.text.isEmpty){
      DialogAlert.showNotification(context, "Thông báo", "Vui lòng nhập mã captcha");
      return;
    }
    if(phoneNumberCtrl.text[0]=='0'){
      phoneNumberCtrl.text = phoneNumberCtrl.text.replaceFirst("0", "84 ");
    }
    Map<String,dynamic> body = {
      "playerid": usernameCtrl.text,
      "password": Hmac(sha1, utf8.encode(usernameCtrl.text)).convert(utf8.encode(passwordCtrl.text)),
      "mobile":phoneNumberCtrl.text,
      "currency":"VND2",
      "language":4,
      "captcha":captchaCtrl.text,
      "captchauuid": captchaModel!.uuid
    };

    dynamic result = await registerService.register(context, body);
    if(result!=null){
      Global.token = result['token'];
      DialogAlert.showNotification(context, "Thông báo", "Đăng ký thành công",onClickOk: (){
        Navigator.pop(context);
      });
    }

    print(body);
  }
}
